/**************************************************************************************

感知器(Perceptron)
初始化: w1 = 1, w2 = 1, H = 2
训练过程:
1.输入样本: (0.1 0.2 -0.5),(0.5 0.6 1.75),(1.0 2.0 5.9),(2.3 1.2 8.4),(3.0 4.0 16.1)
2.更新权值: w = w + step1*error*x (w:权值, step1:步长, error:误差, x:样本)
3.更新阈值: H = H - step2*error (step2: 步长, error: 误差)
4.终止条件: 误差小于0.25

***************************************************************************************/

#include<stdio.h>
#include<math.h>

void main()
{
	double W1=1,W2=1,H=2;
	double  X1, X2, Y, T, s;//Y: 实际输出 T: 期望输出 s: 误差值
        int count = 0;
        do
	{
		    s=0;
	            X1=0.1; X2=0.2; T=-0.5;
		    Y=W1*X1+W2*X2-H;
		    W1=W1+0.05*(T-Y)*X1;
		    W2=W2+0.05*(T-Y)*X2;
		    H=H-0.02*(T-Y);
		    s=s+fabs(T-Y);

		    X1=0.5; X2=0.6; T=1.75;
		    Y=W1*X1+W2*X2-H;
		    W1=W1+0.05*(T-Y)*X1;
		    W2=W2+0.05*(T-Y)*X2;
		    H=H-0.02*(T-Y);
		    s=s+fabs(T-Y);

		    X1=1.0; X2=2.0; T=5.9;
		    Y=W1*X1+W2*X2-H;
		    W1=W1+0.05*(T-Y)*X1;
		    W2=W2+0.05*(T-Y)*X2;
		    H=H-0.02*(T-Y);
		    s=s+fabs(T-Y);

		    X1=2.3;X2=1.2;T=8.4;
		    Y=W1*X1+W2*X2-H;
		    W1=W1+0.05*(T-Y)*X1;
		    W2=W2+0.05*(T-Y)*X2;
		    H=H-0.02*(T-Y);
		    s=s+fabs(T-Y);

		    X1=3.0; X2=4.0; T=16.1;
		    Y=W1*X1+W2*X2-H;
		    W1=W1+0.05*(T-Y)*X1;
		    W2=W2+0.05*(T-Y)*X2;
		    H=H-0.02*(T-Y);
		    s=s+fabs(T-Y);

                    printf("Training counts: %d\n",++count);
                    printf("W1: %f\n",W1);
                    printf("W2: %f\n",W2);
                    printf("Error value: %f\n\n",s);
	}while(s>=0.25);
	
        printf("\n");
        printf("Y = (%f)*X1 + (%f)*X2 - (%f)\n\n",W1,W2,H);
}
